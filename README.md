# aws_ami_cleanup

Simple script to deregister AWS AMIs and purge its snapshots (optionally).

## Name
aws_ami_cleanup.py

## Description
Simple command line script to iterate thru selected AWS regions and find self owned registered AMIs. Then you're prompted which of them should be removed.

## Installation
It works using python3 and boto3, so something like that will do:

```sh
~$ pip install boto3
```

## Authentication
This script relies on shared credentials file or external authentication to access your AWS account.

You can either use `aws configure` to create a valid `~/.aws/credentials` file or set your environment variables to point to your access and secret keys.

Please check [Boto3 documentation](https://boto3.amazonaws.com/v1/documentation/api/latest/guide/credentials.html) if you need more information.

## Usage
You can change the default `args` if you prefer to keep snapshots or you use other regions. Feel free to change it!

```sh
# List available AMIs in Ohio and N.Virginia regions but keep snapshots when deregistering.
~$ ./aws_ami_cleanup.py --region us-east-1 us-east-2 --keep
# List available AMIs in Ireland region to purge them along with snapshots.
~$ ./aws_ami_cleanup.py --region eu-west-1
# Get help.
~$ ./aws_ami_cleanup.py -h
```

## Support
`nil`

## Roadmap
Probably will add other parameters like image-id in command line *BUT* it's not the goal of this script.
Scripting is really easy using aws cli so this script it's intended to be interactive.

## Authors and acknowledgement
`$me` atm.

## License
This is public domain.

## Project status
Didn't find any other similar script so I just wrote my own. Here it is, do what you want with it :)
