#!/usr/bin/env python3

import boto3
import argparse

parser = argparse.ArgumentParser(description='Purge AMIs and optionally its snapshots for selected AWS regions.')
parser.add_argument('-r', '--region', nargs='+',
        help="specify one or more AWS regions, defaults to: us-east-[1|2]")
parser.add_argument('-k', '--keep',
        dest='keep', action='store_true',
        help="keep snapshots when deregistering AMIs")
parser.add_argument('-nk', '--no-keep', 
        dest='keep', action='store_false', 
        help="remove snapshots when deregistering AMIs (default)")
# by default let's purge snapshots and use Ohio/N.Virginia regions. Change it if needed.
parser.set_defaults(keep=False, region=['us-east-1', 'us-east-2']) 
args = parser.parse_args()

if not args.keep:
    print("WARNING: The --keep command line parameter was not specified, snapshots *WILL BE DELETED* along with AMIs!")
    print()

amis = []

# use list->set->list to remove list item duplicates
for region in list(set(args.region)):
    ec2 = boto3.client('ec2', region_name=region)
    amis = amis + list(map(lambda x: dict(x,Region=region), ec2.describe_images(Owners = ['self'])['Images']))

# amis are reverse sorted by creation date
amis = sorted(amis, key=lambda x: x['CreationDate'], reverse=True)

if len(amis) < 1:
    print("No AMIs to work with, exiting.")
    exit(0)

count = 0
for ami in amis:
    print("%3d" % count + ') ' + ami['ImageId'] + " | " + ami['Region'] + ' | ' + ami['Name'])
    count += 1
# get a sorted list of (non repeated) integers
range = sorted(list(set(map(int,input("What AMIs do you want to deregister? (E.g. 1 2 10 11 13), or use '-1' to quit: ").split()))))

if -1 in range:
    print("Exit requested, no actions taken.")
    exit (0)

# deregister amis and (optionally) snapshots selected by user
for item in range:
    if item < len(amis):
        print("Derregistering AMI '%s'..." % amis[item]['ImageId'])
        ec2 = boto3.client('ec2', region_name=amis[item]['Region'])
        ec2.deregister_image(ImageId=amis[item]['ImageId'])
        if not args.keep:
            for snapshot in amis[item]['BlockDeviceMappings']:
                if 'Ebs' in snapshot:
                    print("Found 'Ebs' disk snapshot id '" + snapshot['Ebs']['SnapshotId'] + "', deleting...")
                    ec2.delete_snapshot(SnapshotId=snapshot['Ebs']['SnapshotId'])
    else:
        print("Sorry item '%d' is not in the list, skipping AMI..." % item)